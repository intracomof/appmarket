package mobilemarket.apps.top.appstore.appstoremarket;

/**
 * Created by alexpin on 23.06.17.
 */

public class AppItem {

    private String image;
    private String title;
    private String url;
    private String apk;
    private String adId;

    public AppItem(String image, String title, String url, String apk, String adId) {
        this.image = image;
        this.title = title;
        this.url = url;
        this.apk = apk;
        this.adId = adId;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public String getApk() {
        return apk;
    }

    public void setApk(String apk) {
        this.apk = apk;
    }

    public String getAdId() {
        return adId;
    }

    public void setAdId(String adId) {
        this.adId = adId;
    }
}
