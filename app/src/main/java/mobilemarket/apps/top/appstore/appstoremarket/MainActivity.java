package mobilemarket.apps.top.appstore.appstoremarket;

import android.Manifest;
import android.app.DownloadManager;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.preference.PreferenceManager;
import android.support.design.widget.NavigationView;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.NotificationCompat;
import android.support.v4.content.ContextCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Gravity;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.crashlytics.android.Crashlytics;
import io.fabric.sdk.android.Fabric;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonArrayRequest;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;

import java.util.ArrayList;

import com.google.android.gms.ads.AdListener;
import com.google.android.gms.ads.AdView;
import com.google.android.gms.ads.InterstitialAd;
import com.google.android.gms.ads.MobileAds;
import com.google.android.gms.ads.AdRequest;

public class MainActivity extends AppCompatActivity {

    private static final String INIT_URL = "http://app.allnet.me/appstore-init.php";
    private static final String START_URL = "http://app.allnet.me/appstore-api.php";
    private static final String GET_APPS_URL = "http://app.allnet.me/appstore-api.php?category_id=";
    private static final String TITLE_DOWNLOAD_COMPLETED = "Download completed";
    private static final int LAYOUT = R.layout.activity_main;
    private Toolbar toolbar;
    private DrawerLayout drawerLayout;
    private ListView itemsList;
    private CustomListAdapter itemsAdapter;

    protected static int activeCatId = 1;
    protected static int notifyId = 1;

    protected static String fileNameDownloaded = "";

    private Context context = this;
    private ArrayList<AppItem> AppsList;
    private static final int PERMISSION_REQUEST_CODE = 7;

    private static final int NOTIFCATION_CODE_NOT_USED = 9;
    private long enqueue;
    private DownloadManager dm;

    private int SHOW_RATE_US = 0;
    private String RATE_US_MESSAGE = "Give us 5 stars and you'll be able to download apps for free";
    private String appPackagenameForRate = "music.mp3.apps.top.download";
    private SharedPreferences mPreferences;

    private static int SHOW_ADS = 0;

    private static String ACC_ID    = "ca-app-pub-5525035995496459~8460307327";
    private static String BANNER_ID = "ca-app-pub-5525035995496459/7142547729";
    private static String PLAKAT_ID = "ca-app-pub-5525035995496459/2413773720";


    private InterstitialAd mInterstitialAd;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        setTheme(R.style.AppDefault);
        super.onCreate(savedInstanceState);
        Fabric.with(this, new Crashlytics());
        mPreferences = PreferenceManager.getDefaultSharedPreferences(this);

        getInitParams();
        setContentView(LAYOUT);
        initToolbar();

    }

    private void afterInit() {
        if(SHOW_RATE_US == 1) {
            showRateUsDialog();
        }
        if(SHOW_ADS == 1) {
            initAds();
        }
        initNavigationView();
        initBroadcast();
    }

    private void initAds() {
        MobileAds.initialize(this, ACC_ID);
        mInterstitialAd = new InterstitialAd(this);
        mInterstitialAd.setAdUnitId(PLAKAT_ID);
        mInterstitialAd.loadAd(new AdRequest.Builder().build());
        mInterstitialAd.setAdListener(new AdListener() {
            @Override
            public void onAdClosed() {
                mInterstitialAd.loadAd(new AdRequest.Builder().build());
            }

        });

        AdView mAdView = (AdView) findViewById(R.id.adView);
        AdRequest adRequest = new AdRequest.Builder().build();
        mAdView.loadAd(adRequest);
    }

    private void getInitParams() {
        JsonObjectRequest jsObjRequest = new JsonObjectRequest(Request.Method.GET, INIT_URL, null, new Response.Listener<JSONObject>() {

            @Override
            public void onResponse(JSONObject response) {
                try {

                    JSONObject appsObj = response.getJSONObject("data");

                    SHOW_RATE_US = appsObj.getInt("show_rate_us");
                    RATE_US_MESSAGE = appsObj.getString("rate_us_text");
                    appPackagenameForRate = appsObj.getString("rate_pckg");
                    SHOW_ADS = appsObj.getInt("show_ads");

                    afterInit();
                } catch (JSONException e) {
                    e.printStackTrace();
                }

            }
        }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error) {

            }
        });

        RequestQueue queue = Volley.newRequestQueue(this);
        queue.add(jsObjRequest);
    }

    private void initToolbar() {
        toolbar = (Toolbar) findViewById(R.id.toolbar);
        toolbar.setTitle(R.string.app_name);

    }

    private void initNavigationView() {
        drawerLayout = (DrawerLayout) findViewById(R.id.drawer_layout);

        ActionBarDrawerToggle toogle = new ActionBarDrawerToggle(this, drawerLayout, toolbar, R.string.open, R.string.close);
        drawerLayout.setDrawerListener(toogle);
        toogle.syncState();
        NavigationView navigationView = (NavigationView) findViewById(R.id.navigation);
        setCategories(navigationView);

        AppsList = new ArrayList<>();


        final ListView itemsList = (ListView) findViewById(R.id.items_list);
        itemsAdapter = new CustomListAdapter(getApplicationContext(), R.layout.app_item, AppsList);
        itemsList.setAdapter(itemsAdapter);

        itemsList.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            public void onItemClick(AdapterView<?> parent, View view,
                                    int position, long id) {
                if (AppsList.get((int) id).getApk().equals("")) {
                    if(checkPermissions()) {
                        String fileURL = AppsList.get((int) id).getUrl();
                        fileNameDownloaded = AppsList.get((int) id).getTitle();
                        dm = (DownloadManager) getSystemService(DOWNLOAD_SERVICE);
                        DownloadManager.Request request = new DownloadManager.Request(
                                Uri.parse(fileURL));
                        request.setMimeType("*/*");
                        request.setVisibleInDownloadsUi(true);
                        request.setDestinationInExternalPublicDir(
                                Environment.DIRECTORY_DOWNLOADS, getFilenameFromUrl(fileURL));
                        enqueue = dm.enqueue(request);
                        showToast(fileNameDownloaded + " is downloading");
                    }

                } else {
                    Intent intent = new Intent(Intent.ACTION_VIEW);
                    intent.setData(Uri.parse("market://details?id=" + AppsList.get((int) id).getApk()));
                    startActivity(intent);
                }

            }
        });

        getAppsList();

        navigationView.setNavigationItemSelectedListener(new NavigationView.OnNavigationItemSelectedListener() {
            @Override
            public boolean onNavigationItemSelected(MenuItem menuItem) {
                if(SHOW_ADS == 1) {
                    if (mInterstitialAd.isLoaded()) {
                        mInterstitialAd.show();
                    }
                }
                drawerLayout.closeDrawers();
                toolbar.setTitle(menuItem.getTitle());
                activeCatId = menuItem.getItemId();
                getAppsList();
                return true;
            }
        });

    }

    private void getAppsList() {
        String url = GET_APPS_URL + activeCatId;

        JsonObjectRequest jsObjRequest = new JsonObjectRequest(Request.Method.GET, url, null, new Response.Listener<JSONObject>() {

            @Override
            public void onResponse(JSONObject response) {
                try {
                    AppsList.clear();
                    JSONArray appsObj = response.getJSONArray("apps");
                    for (int i = 0; i < appsObj.length(); i++) {
                        JSONObject appItem = (JSONObject) appsObj.get(i);
                        AppsList.add(i, new AppItem(
                                appItem.getString("app_img"),
                                appItem.getString("app_name"),
                                appItem.getString("app_link"),
                                appItem.getString("package"),
                                BANNER_ID
                        ));
                    }
                    itemsAdapter.notifyDataSetChanged();
                } catch (JSONException e) {
                    e.printStackTrace();
                }

            }
        }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error) {

            }
        });

        RequestQueue queue = Volley.newRequestQueue(this);
        queue.add(jsObjRequest);
    }


    /**
     * @param navigationView
     */
    public void setCategories(final NavigationView navigationView) {
        JsonArrayRequest jsObjRequest = new JsonArrayRequest(Request.Method.GET, START_URL, null, new Response.Listener<JSONArray>() {

            @Override
            public void onResponse(JSONArray response) {
                try {
                    activeCatId = ((JSONObject) response.get(0)).getInt("category_id");

                    for (int inc = 0; inc < response.length(); inc++) {
                        JSONObject category = (JSONObject) response.get(inc);
                        navigationView.getMenu().add(1, category.getInt("category_id"), Menu.NONE, category.getString("category_name"));
                    }
                } catch (JSONException e) {
                    e.printStackTrace();

                }

            }
        }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error) {


            }
        });

        RequestQueue queue = Volley.newRequestQueue(this);
        queue.add(jsObjRequest);
    }

    private void getDialog(String message) {
        AlertDialog.Builder builder1 = new AlertDialog.Builder(context);
        builder1.setMessage(message);
        builder1.setCancelable(true);

        builder1.setPositiveButton(
                "Yes",
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        dialog.cancel();
                    }
                });

        builder1.setNegativeButton(
                "No",
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        dialog.cancel();
                    }
                });
        AlertDialog alert11 = builder1.create();
        alert11.show();
    }

    private void showRateUsDialog() {
        if (mPreferences.getBoolean("rated", false)) return;
        AlertDialog.Builder builder1 = new AlertDialog.Builder(context);

        builder1.setMessage(RATE_US_MESSAGE);

        builder1.setCancelable(true);

        builder1.setPositiveButton(
                "Rate this app",
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        mPreferences.edit().putBoolean("rated", true).apply();
                        Intent intent = new Intent(Intent.ACTION_VIEW);
                        intent.setData(Uri.parse("market://details?id=" + appPackagenameForRate));
                        startActivity(intent);
                    }
                });

        builder1.setNegativeButton(
                "Maybe later",
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        dialog.cancel();
                    }
                });
        AlertDialog alert11 = builder1.create();

        alert11.show();
        TextView textView = (TextView) alert11.findViewById(android.R.id.message);
        textView.setTextSize(24);
    }


    private boolean checkPermissions() {
        if (ContextCompat.checkSelfPermission(this, Manifest.permission.WRITE_EXTERNAL_STORAGE)
                != PackageManager.PERMISSION_GRANTED ||
                ContextCompat.checkSelfPermission(this, Manifest.permission.READ_EXTERNAL_STORAGE)
                        != PackageManager.PERMISSION_GRANTED) {
                requestPermissions();
            return false;
        } else  {
            return true;
        }
    }

    private void requestPermissions() {
        ActivityCompat.requestPermissions(this,
                new String[]{
                        Manifest.permission.READ_EXTERNAL_STORAGE,
                        Manifest.permission.WRITE_EXTERNAL_STORAGE
                },
                PERMISSION_REQUEST_CODE);
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
    }


    public void initBroadcast() {
        BroadcastReceiver receiver = new BroadcastReceiver() {
            @Override
            public void onReceive(Context context, Intent intent) {
                String action = intent.getAction();
                if (DownloadManager.ACTION_DOWNLOAD_COMPLETE.equals(action)) {
                    long downloadId = intent.getLongExtra(
                            DownloadManager.EXTRA_DOWNLOAD_ID, 0);
                    DownloadManager.Query query = new DownloadManager.Query();
                    query.setFilterById(enqueue);
                    Cursor c = dm.query(query);
                    if (c.moveToFirst()) {
                        int columnIndex = c
                                .getColumnIndex(DownloadManager.COLUMN_STATUS);
                        if (DownloadManager.STATUS_SUCCESSFUL == c
                                .getInt(columnIndex)) {
                            DownloadManager downloadManager = DownloadManager.class.cast(getSystemService(DOWNLOAD_SERVICE));
                            Uri uri = downloadManager.getUriForDownloadedFile(downloadId);
                            showNotification(TITLE_DOWNLOAD_COMPLETED, "Download completed " + fileNameDownloaded, uri);
                            showToast("Downloading is completed: " + fileNameDownloaded);
                        }
                    }
                }
            }
        };

        registerReceiver(receiver, new IntentFilter(
                DownloadManager.ACTION_DOWNLOAD_COMPLETE));
    }

    public void showNotification(String title, String text, Uri localUri) {
        final Intent emptyIntent = new Intent(Intent.ACTION_OPEN_DOCUMENT);
        emptyIntent.addCategory(Intent.CATEGORY_OPENABLE);
        emptyIntent.setType("*/*");
        //emptyIntent.setDataAndType(localUri, "application/vnd.android.package-archive");

        PendingIntent pendingIntent = PendingIntent.getActivity(this, NOTIFCATION_CODE_NOT_USED, emptyIntent, PendingIntent.FLAG_UPDATE_CURRENT);

        NotificationCompat.Builder mBuilder =
                new NotificationCompat.Builder(this)
                        .setSmallIcon(R.drawable.app_store_icon)
                        .setContentTitle(title)
                        .setContentText(text)
                        .setContentIntent(pendingIntent);

        NotificationManager notificationManager = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
        notificationManager.notify(notifyId++, mBuilder.build());

    }

    public void showToast(String message) {
        Toast toast = Toast.makeText(getApplicationContext(),
                message,
                Toast.LENGTH_SHORT);
        toast.setGravity(Gravity.CENTER, 0, 0);
        toast.show();
    }

    public String getFilenameFromUrl(String url)
    {
        int cut = url.lastIndexOf('/');
        return url.substring(cut+1);
    }
}

