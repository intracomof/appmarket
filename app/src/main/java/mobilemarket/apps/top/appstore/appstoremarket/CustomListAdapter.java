package mobilemarket.apps.top.appstore.appstoremarket;

import android.app.Activity;
import android.content.Context;
import android.support.annotation.LayoutRes;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.AdSize;
import com.google.android.gms.ads.AdView;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.List;

public class CustomListAdapter extends ArrayAdapter<AppItem> {

    ArrayList<AppItem> appItems;
    Context context;
    int resource;

    public CustomListAdapter(@NonNull Context context, @LayoutRes int resource, @NonNull ArrayList<AppItem> appItems) {
        super(context, resource, appItems);
        this.appItems = appItems;
        this.context = context;
        this.resource = resource;
    }

    @NonNull
    @Override
    public View getView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {

        if(convertView == null) {
            LayoutInflater layoutInflater = (LayoutInflater) getContext().
                    getSystemService(Activity.LAYOUT_INFLATER_SERVICE);
            convertView = layoutInflater.inflate(R.layout.app_item, null, true);
        }

        AppItem appItem = getItem(position);

        ImageView imageView = (ImageView) convertView.findViewById(R.id.imageViewAppItem);
        Picasso.with(context).load(appItem.getImage()).into(imageView);

        TextView title = (TextView) convertView.findViewById(R.id.titleAppItem);
        title.setText(appItem.getTitle());

//        AdView mAdView = (AdView) convertView.findViewById(R.id.adView);
//        AdRequest adRequest = new AdRequest.Builder().build();
//        mAdView.loadAd(adRequest);

        return convertView;
    }
}
